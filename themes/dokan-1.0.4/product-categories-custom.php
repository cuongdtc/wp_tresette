<?php
/**
 * Template Name: Product category
 * 
 * Template for show product category
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dokan
 * @package dokan - 2014 1.0
 */
get_header ();
?>

<div class="row" style="margin-top: 10px;">
	<div class="col-md-3">
                <?php dokan_category_widget(); ?>
            </div>

	<div class="col-md-9">
		<?php while (have_posts()) : the_post(); ?>

            <?php get_template_part( 'content', 'page' ); ?>

            <?php
									// If comments are open or we have at least one comment, load up the comment template
									if (comments_open () || '0' != get_comments_number ())
										comments_template ( '', true );
									?>

        <?php endwhile; // end of the loop. ?>
	</div>
</div>
<!-- #home-page-section-1 -->


<?php get_footer(); ?>