<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package dokan
 * @package dokan - 2014 1.0
 */
?>
</div>
<!-- .row -->
</div>
<!-- .container -->
</div>
<!-- #main .site-main -->
<footer id="colophon" class="site-footer" role="contentinfo">
<?php echo do_shortcode('[tawkto]'); ?>
	<div class="footer-widget-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 none-sp">
                    <div class="footer-logo">
	                    <?php dynamic_sidebar( 'footer-1' ); ?>
                    </div>
                </div>

				<div class="col-lg-3 col-md-4 col-sm-3 col-xs-5 col-mb-12">
                    <div class="footer-item">
	                    <?php dynamic_sidebar( 'footer-2' ); ?>
                    </div>
                </div>

				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-mb-12">
                    <div class="footer-item">
	                    <?php dynamic_sidebar( 'footer-3' ); ?>
                    </div>
                </div>

				<div class="col-lg-3 col-md-5 col-sm-6 col-xs-2 col-mb-12">
                    <div class="footer-fb-fanpage">
                        <div class="footer-fb-link">
                            <?php dynamic_sidebar( 'footer-4' ); ?>
                        </div>
                        <div class="footer-fb-button">
                            <a href="https://www.facebook.com/Tresettehanoi/" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
			</div>
			<!-- .footer-widget-area -->
		</div>
	</div>

	<div class="copy-container">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="footer-copy"></div>
				</div>
			</div>
		</div>
		<!-- .row -->
	</div>
	<!-- .container -->
	</div>
	<!-- .copy-container -->
</footer>
<!-- #colophon .site-footer -->
</div>
<!-- #page .hfeed .site -->
  <?php dynamic_sidebar( 'facebook-like-page' ); ?>
<?php wp_footer(); ?>

<div id="yith-wcwl-popup-message" style="display: none;">
	<div id="yith-wcwl-message"></div>
</div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1694223360867361',
      xfbml      : true,
      version    : 'v2.5'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery(".btn_addtocart").click(function(){
			var url = jQuery(this).attr("href");
			var productId = jQuery(this).attr("data-product_id");
			jQuery.ajax({
				  method: "GET",
				  url: url,
				  beforeSend: function() {
					  jQuery("#popup").show();	
					  jQuery("#AjaxLoading").show();
					}
			}).done(function( msg ) {
				var cartproducttotal = jQuery("#cartproducttotal").text();
				var newtotal = parseInt(cartproducttotal) + 1;
				jQuery("#cartproducttotal").text(newtotal) ;
				jQuery.ajax({
					   url: "<?php echo get_permalink('1081') . "?productid="; ?>" + productId,
					   type:'GET',
					   success: function(data){
						   jQuery('#popupcontent').html(data);
						   jQuery("#AjaxLoading").hide();
					   }
				});
			});	
			return false;
		});
	});
	function closepopup(){
		jQuery("#popup").hide();
		jQuery('#popupcontent').html("");		
	}
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5a6ec5ce4b401e45400c78ab/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
<div id="popup" >
	
	<div id="popupcontent"></div>
</div>
<div id="AjaxLoading" style="display:none; " >
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/ajax-load.gif">&nbsp; Đang tải... Vui lòng chờ...</div>
</body>
</html>