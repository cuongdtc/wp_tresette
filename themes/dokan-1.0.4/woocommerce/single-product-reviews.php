<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.2
 */
global $product;

if (! defined ( 'ABSPATH' )) {
	exit (); // Exit if accessed directly
}

if (! comments_open ()) {
	return;
}

?>
<div id="reviews">
	<div id="comments">
		<h2><?php
		if (get_option ( 'woocommerce_enable_review_rating' ) === 'yes' && ($count = $product->get_review_count ()))
			printf ( _n ( 'Sản phẩm %s đã có %s lần đánh giá', 'Sản phẩm %s đã có %s lần nhận xét', 'woocommerce', $count ), get_the_title (), $count );
		else
			_e ( 'Đánh giá sản phẩm', 'woocommerce' );
		?></h2>

		<?php if ( have_comments() ) : ?>

			<ol class="commentlist">
				<?php wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array( 'callback' => 'woocommerce_comments' ) ) ); ?>
			</ol>

			<?php
			
if (get_comment_pages_count () > 1 && get_option ( 'page_comments' )) :
				echo '<nav class="woocommerce-pagination">';
				paginate_comments_links ( apply_filters ( 'woocommerce_comment_pagination_args', array (
						'prev_text' => '&larr;',
						'next_text' => '&rarr;',
						'type' => 'list' 
				) ) );
				echo '</nav>';
			
			endif;
			?>

		<?php else : ?>

		<!-- <p class="woocommerce-noreviews"><?php _e( 'Sản phẩm chưa được đánh giá', 'woocommerce' ); ?></p>  -->	

		<?php endif; ?>
	</div>

	<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->id ) ) : ?>

		<div id="review_form_wrapper">
		<div id="review_form">
				<?php
		$commenter = wp_get_current_commenter ();
		
		$comment_form = array (
				'title_reply' => have_comments () ? __ ( 'Hãy cho chúng tôi biết nhận xét của bạn về sản phẩm này.', 'woocommerce' ) : sprintf ( __ ( 'Hãy là người đầu tiên nhận xét sản phẩm &ldquo;%s&rdquo;', 'woocommerce' ), get_the_title () ),
				'title_reply_to' => __ ( 'Leave a Reply to %s', 'woocommerce' ),
				'comment_notes_before' => '',
				'comment_notes_after' => '',
				'fields' => array (
						'author' => '<p class="comment-form-author">' . '<label for="author">' . __ ( 'Name', 'woocommerce' ) . ' <span class="required">*</span></label> ' . '<input id="author" name="author" type="text" value="' . esc_attr ( $commenter ['comment_author'] ) . '" size="30" aria-required="true" /></p>',
						'email' => '<p class="comment-form-email"><label for="email">' . __ ( 'Email', 'woocommerce' ) . ' <span class="required">*</span></label> ' . '<input id="email" name="email" type="text" value="' . esc_attr ( $commenter ['comment_author_email'] ) . '" size="30" aria-required="true" /></p>' 
				),
				'label_submit' => __ ( 'Đăng nhận xét', 'woocommerce' ),
				'logged_in_as' => '',
				'comment_field' => '' 
		);
		
		if ($account_page_url = wc_get_page_permalink ( 'myaccount' )) {
			$comment_form ['must_log_in'] = '<p class="must-log-in">' . sprintf ( __ ( 'Bạn cần <a href="%s">đăng nhập</a> để nhận xét sản phẩm này', 'woocommerce' ), esc_url ( $account_page_url ) ) . '</p>';
		}
		
		if (get_option ( 'woocommerce_enable_review_rating' ) === 'yes') {
			$comment_form ['comment_field'] = '<p class="comment-form-rating"><label for="rating">' . __ ( 'Đánh giá của bạn', 'woocommerce' ) . '</label><select name="rating" id="rating">
							<option value="">' . __ ( 'Rate&hellip;', 'woocommerce' ) . '</option>
							<option value="5">' . __ ( 'Perfect', 'woocommerce' ) . '</option>
							<option value="4">' . __ ( 'Good', 'woocommerce' ) . '</option>
							<option value="3">' . __ ( 'Average', 'woocommerce' ) . '</option>
							<option value="2">' . __ ( 'Not that bad', 'woocommerce' ) . '</option>
							<option value="1">' . __ ( 'Very Poor', 'woocommerce' ) . '</option>
						</select></p>';
		}
		
		$comment_form ['comment_field'] .= '<p class="comment-form-comment"><label for="comment">' . __ ( 'Nhận xét của bạn', 'woocommerce' ) . '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>';
		
		comment_form ( apply_filters ( 'woocommerce_product_review_comment_form_args', $comment_form ) );
		?>
			</div>
	</div>

	<?php else : ?>

		<p class="woocommerce-verification-required"><?php _e( 'Chỉ khách hàng đăng nhập mua sản phẩm này mới được nhận xét.', 'woocommerce' ); ?></p>

	<?php endif; ?>

	<div class="clear"></div>
</div>
