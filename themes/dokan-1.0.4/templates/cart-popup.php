<?php
/**
 * A full width page template
 *
 * Template Name: AAA Popup
 *
 * @package dokan
 * @package dokan - 2013 1.0
 */
?>
<a id="closepopup" onclick="closepopup()">X</a>
<?php
$id = $_GET ['productid'];
$p = new WC_Product ( $id );

?>
<div class="row">
	<h2>1 Sản phẩm đã được thêm vào giỏ hàng.</h2>
	<div class="col-md-5 singleproduct">
		<a href="<?php echo $p->get_permalink(); ?>" class="img"><?php echo $p->get_image('thumbnail');?></a>
		<p>
			<a href="<?php echo $p->get_permalink(); ?>"><?php echo $p->get_title(); ?></a>
		</p>
		<p>
			<a class="price"><?php echo number_format($p->get_price(), 0, ',', '.'); ?> đ</a>
		</p>
		<p>Số lượng: 1</p>
	</div>
	<div class="col-md-7">
		<div
			style="border: 1px solid #d3d3d3; color: #494c50; font-size: 16px; float: right; padding: 12px; text-align: right; width: 473px;">
			<p>Trị giá đơn hàng: <strong style="    color: #e74c3c; "><?php echo WC()->cart->get_cart_total(); ?></strong></p>
			<p>Giỏ hàng của bạn hiện có <?php echo WC()->cart->get_cart_contents_count(); ?> sản phầm.</p>
		</div>
		<div class="order-button-container">
			<a href="<?php echo get_permalink(6); ?>" class="button"><span>Đặt
					hàng và thanh toán</span></a> <a
				href="<?php echo get_permalink(5); ?>" class="button"><span>Xem giỏ
					hàng</span></a> <a href="javascript:;" class="button"
				onclick="closepopup();"><span>Mua thêm sản phẩm</span></a>
		</div>
	</div>
</div>
<hr>
<div class="row productrelative">
	<?php
	
$latest_query = new WP_Query ( array (
			'posts_per_page' => 4,
		'orderby'          => 'rand',
			'post_type' => 'product',
			
	) );
	?>
                        <?php while ( $latest_query->have_posts() ) : $latest_query->the_post(); ?>

                            <?php wc_get_template_part( 'content', 'product' ); ?>

                        <?php endwhile; ?>
</div>