<?php
/**
 * The main template file for homepage.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dokan
 * @package dokan - 2014 1.0
 */
get_header ();
?>

<div class="row">
	<div class="home-content-area col-xs-12">
		<?php
		if (dokan_get_option ( 'show_slider', 'dokan_home', 'on' ) == 'on') {
			$slider_id = dokan_get_option ( 'slider_id', 'dokan_home', '-1' );

			if ($slider_id != '-1') {
				Dokan_Slider::init ()->get_slider ( $slider_id );
			}

			do_action ( 'dokan_home_on_slider' );
		}
		?>
	</div>
</div>
<?php
	if(function_exists('widget_product_home')):
		dynamic_sidebar( 'home-product' );
?>
<?php else: ?>
	<div class="row" style="margin-top: 10px;">

		<div class="col-xs-12">
			<div class="row productlist">
				<?php echo do_shortcode('[featured_products per_page="-1" columns="4"]'); ?>
			</div>
		</div>
	</div>
<?php endif; ?>

<!-- #home-page-section-1 -->


<?php get_footer(); ?>