<?php
if (! function_exists ( 'WC' )) {
	wp_die ( sprintf ( __ ( 'Please install <a href="%s"><strong>WooCommerce</strong></a> plugin first', 'dokan' ), 'http://wordpress.org/plugins/woocommerce/' ) );
}
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package dokan
 * @package dokan - 2014 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="shortcut icon"
	href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head(); ?>
<script type="text/javascript">
	
	jQuery(document).ready(function(){
		var hidePopup = getCookie("hide_popup");
		if (hidePopup!="") {
			jQuery("#hd_pop").hide();
		}
		    
		jQuery("button.hd_pops_close").click(function(){
			jQuery("#hd_pop").hide();
		});
		jQuery("button.hd_pops_reject").click(function(){
			jQuery("#hd_pop").hide();
			setCookie('hide_popup', '12', 1);
		});
	});
	
	function setCookie(cname, cvalue, exdays) {
	    var d = new Date();
	    d.setTime(d.getTime() + (exdays*24*60*60*1000));
	    var expires = "expires="+d.toUTCString();
	    document.cookie = cname + "=" + cvalue + "; " + expires;
	}
	
	function getCookie(cname) {
	    var name = cname + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0; i<ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1);
	        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
	    }
	    return "";
	}
	
</script>
</head>

<body <?php body_class(); ?>>
	<div id="hd_pop">
		<?php dynamic_sidebar( 'thongbao-widget' )?>
		<strong style=";font-size:15px;line-height:20.8px;text-align:center;">
		<img alt="yes" height="20" src="http://www.kgimi.co.kr/plugin/editor/ckeditor4/plugins/smiley/images/thumbs_up.gif" title="yes" width="20"><img alt="yes" height="20" src="http://www.kgimi.co.kr/plugin/editor/ckeditor4/plugins/smiley/images/thumbs_up.gif" title="yes" width="20"><img alt="yes" height="20" src="http://www.kgimi.co.kr/plugin/editor/ckeditor4/plugins/smiley/images/thumbs_up.gif" title="yes" width="20"></strong>
		<div class="hd_pops_footer">
            <button class="hd_pops_reject hd_pops_5 24">Không hiển thị trong 24 giờ tới.</button>
            <button class="hd_pops_close hd_pops_5">Đóng</button> <!--닫기-->
        </div>
	</div>
	<div id="page" class="hfeed site">
        <?php do_action( 'before' ); ?>

        <nav class="navbar navbar-inverse navbar-top-area navbar-top-bar">
			<div class="container">
				<div class="row">
<!--					<div class="col-sm-3">-->
<!--						<div class="navbar-header">-->
<!--							<button type="button" class="navbar-toggle"-->
<!--								data-toggle="collapse" data-target=".navbar-top-collapse">-->
<!--								<span class="sr-only">--><?php //_e( 'Toggle navigation', 'dokan' ); ?><!--</span>-->
<!--								<i class="fa fa-bars"></i>-->
<!--							</button>-->
<!--						</div>-->
<!--					</div>-->
					<div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="collapse navbar-collapse navbar-top-collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <!-- <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php printf( __( 'Giỏ hàng %s', 'dokan' ), '<span class="dokan-cart-amount-top">(' . WC()->cart->get_cart_total() . ')</span>' ); ?> <b
										class="caret"></b></a>

									<ul class="dropdown-menu">
										<li>
											<div class="widget_shopping_cart_content"></div>
										</li>
									</ul></li> -->
                                        <?php $current_user= wp_get_current_user(); $enableSelling = $current_user->dokan_enable_selling; ?>
                                        <?php if ( is_user_logged_in() && $enableSelling == 'yes' ) { ?>

                                            <?php
                                            global $current_user;

                                            $user_id = $current_user->ID;
                                            if (dokan_is_user_seller ( $user_id )) {
                                                ?>
                                                <li class="dropdown"><a href="#"
                                                                        class="dropdown-toggle" data-toggle="dropdown"><?php _e( 'Quản lý cửa hàng', 'dokan' ); ?> <b
                                                                class="caret"></b></a>

                                                    <ul class="dropdown-menu">
                                                        <li><a href="<?php echo dokan_get_store_url( $user_id ); ?>"
                                                               target="_blank"><?php _e( 'Visit your store', 'dokan' ); ?> <i
                                                                        class="fa fa-external-link"></i></a></li>
                                                        <li class="divider"></li>
                                                        <?php
                                                        $nav_urls = dokan_get_dashboard_nav ();

                                                        foreach ( $nav_urls as $key => $item ) {
                                                            printf ( '<li><a href="%s">%s &nbsp;%s</a></li>', $item ['url'], $item ['icon'], $item ['title'] );
                                                        }
                                                        ?>
                                                    </ul></li>
                                            <?php } ?>

                                            <li class="dropdown"><a href="#"
                                                                    class="dropdown-toggle" data-toggle="dropdown">Tài khoản của
                                                    tôi<b class="caret"></b>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="<?php echo dokan_get_page_url( 'my_orders' ); ?>"><?php _e( 'Quản lý đơn hàng', 'dokan' ); ?></a></li>
                                                    <li><a
                                                                href="<?php echo dokan_get_page_url( 'myaccount', 'woocommerce' ); ?>"><?php _e( 'Tài khoản của tôi', 'dokan' ); ?></a></li>
                                                    <li><a href="<?php echo wc_customer_edit_account_url(); ?>"><?php _e( 'Chỉnh sửa tài khoản', 'dokan' ); ?></a></li>
                                                    <li class="divider"></li>
                                                    <li><a
                                                                href="<?php echo wc_get_endpoint_url( 'edit-address', 'billing', get_permalink( wc_get_page_id( 'myaccount' ) ) ); ?>"><?php _e( 'Địa chỉ thanh toán', 'dokan' ); ?></a></li>
                                                    <li><a
                                                                href="<?php echo wc_get_endpoint_url( 'edit-address', 'shipping', get_permalink( wc_get_page_id( 'myaccount' ) ) ); ?>"><?php _e( 'Địa chỉ giao hàng', 'dokan' ); ?></a></li>
                                                </ul></li>

                                            <li><?php wp_loginout( home_url() ); ?></li>

                                        <?php } else { ?>
                                            <li><a
                                                        href="<?php echo dokan_get_page_url( 'myaccount', 'woocommerce' ); ?>"><?php _e( 'Đăng nhập', 'dokan' ); ?></a></li>
                                            <li><a
                                                        href="<?php echo dokan_get_page_url( 'myaccount', 'woocommerce' ); ?>"><?php _e( 'Đăng ký', 'dokan' ); ?></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <?php dynamic_sidebar( 'sidebar-header' )?>
                                <div class="header-cart">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                    <span id="cartproducttotal"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                                    <?php dynamic_sidebar( 'header-card' )?>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
		</nav>

		<header id="masthead" class="site-header" role="banner">
			<div class="container">
				<div class="table header-main">
					<div class="table-cell header-logo">
						<hgroup>
							<h1 class="site-title">
								<a href="<?php echo home_url( '/' ); ?>"
									title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
									rel="home"><img
									src="<?php echo get_theme_mod( 'dokan_logo' ) ; ?>"
									alter="<?php bloginfo( 'name' ); ?>"
									title="<?php bloginfo( 'name' ); ?>" /></a>
							</h1>
						</hgroup>
					</div>
					<div class="table-cell header-search">
                        <div class="search_product">
                            <?php dynamic_sidebar( 'menu-sidebar' )?>
                        </div>
					</div>
					<!-- .col-md-6 -->
				</div>
				<!-- .row -->
			</div>
			<!-- .container -->
			<div class="menu-container">
				<div class="container">
					<div class="row">
                        <nav role="navigation" class="site-navigation main-navigation clearfix">
                            <h3 class="assistive-text">
                                <i class="icon-reorder"></i> <?php _e( 'Menu', 'dokan' ); ?></h3>
                            <div class="assistive-text skip-link">
                                <a href="#content"
                                    title="<?php esc_attr_e( 'Skip to content', 'dokan' ); ?>"><?php _e( 'Skip to content', 'dokan' ); ?></a>
                            </div>
                            <nav class="navbar navbar-default" role="navigation">
<!--                                <div class="navbar-header">-->
<!--                                    <button type="button" class="navbar-toggle"-->
<!--                                        data-toggle="collapse" data-target=".navbar-main-collapse">-->
<!--                                        <span class="sr-only">--><?php //_e( 'Toggle navigation', 'dokan' ); ?><!--</span>-->
<!--                                        <i class="fa fa-bars"></i>-->
<!--                                    </button>-->
<!--										<a class="navbar-brand" href="--><?php //echo home_url(); ?><!--"><i-->
<!--											class="fa fa-home"></i> --><?php //_e( 'Trang chủ', 'dokan' ); ?><!--</a>-->
<!--                                </div>-->
                                <?php
                                    wp_nav_menu ( array (
                                        'theme_location' => 'primary',
                                        'container' => 'div',
                                        'container_class' => 'navbar-collapse navbar-main-collapse',
                                        'menu_class' => 'nav navbar-nav',
                                        'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                        'walker' => new wp_bootstrap_navwalker ()
                                    ) );
                                ?>

                            </nav>
                        </nav>
							<!-- .site-navigation .main-navigation -->
					</div>
					<!-- .row -->
				</div>
				<!-- .container -->
			</div>
			<!-- .menu-container -->
		</header>
		<!-- #masthead .site-header -->

		<div id="main" class="site-main">
			<div class="container content-wrap">
				<div class="row">