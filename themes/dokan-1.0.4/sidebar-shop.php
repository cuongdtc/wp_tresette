<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package dokan
 * @package dokan - 2014 1.0
 */
?>
<div id="secondary" class="col-md-3 clearfix" role="complementary">

	<button type="button" class="navbar-toggle widget-area-toggle"
		data-toggle="collapse" data-target=".widget-area">
		<i class="fa fa-bars"></i> <span class="bar-title"><?php _e( 'Toggle Sidebar', 'dokan' ); ?></span>
	</button>

	<div class="widget-area collapse widget-collapse">

         <?php dokan_category_widget(); ?>
    </div>
</div>
<!-- #secondary .widget-area -->
