<?php

class Categories
{
    public function getCategory($category, $inputField) {
        $category = (array)$category;
        $this->recurveCate(get_terms('product_cat'), $parrent_id = 0, $txt, $category, $inputField);
    }

    public function recurveCate($array, $parrent_id = 0, $txt = '', $category, $inputField) {
        foreach ($array as $key => $value) {
            if($value->parent == $parrent_id) {
                if(in_array($value->slug, $category)) {
                    echo "<p><input value='". $value->slug ."' type='checkbox' name='". $inputField ."[]' checked /><label>" . $txt . $value->name . "</label></p>";
                }else {
                    echo "<p><input value='". $value->slug ."' type='checkbox' name='". $inputField ."[]' /><label>" . $txt . $value->name . "</label></p>";
                }
                $this->recurveCate($array, $value->term_id, $txt . '--', $category, $inputField);
                unset($array[$key]);
            }
        }
    }
}
