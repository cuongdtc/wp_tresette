<?php

/**
 * Create class Rioland_Widget
 */

add_action( 'widgets_init', 'create_product_widget4' );
function create_product_widget4() {
        register_widget('Product_Widget4');
}

class Product_Widget4 extends WP_Widget {
        
    function __construct() {
        parent::__construct (
            'product_widget4', // id of widget
            'Product4', // name of widget
            ['description' => 'widget for product home page']
        );
    }

    /**
     * Form of widget
     */
    function form( $instance ) {
        $default = [
            'title' => 'Tiêu đề',
            'post_number' => '8',
            'colunm' => '4',
            'category' => [],
            'type' => '0',
        ];
        $instance = wp_parse_args( (array) $instance, $default);
        $title = esc_attr( $instance['title'] );
        $post_number = esc_attr($instance['post_number']);
        $colunm = esc_attr($instance['colunm']);
        $category = (array)$instance['category'];
        $type = esc_attr($instance['type']);
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'dokan' ); ?></label>
            <input class="widefat"
                   id="<?php echo $this->get_field_id( 'title' ); ?>"
                   name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
                   value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'post_number' ); ?>"><?php _e( 'Số sản phẩm hiển thị:', 'dokan' ); ?></label>
            <input class="widefat"
                   id="<?php echo $this->get_field_id( 'post_number' ); ?>"
                   name="<?php echo $this->get_field_name( 'post_number' ); ?>" type="number"
                   value="<?php echo esc_attr( $post_number ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'colunm' ); ?>"><?php _e( 'Số sản phẩm hiển thị trên dòng:', 'dokan' ); ?></label>
            <select name="<?php echo $this->get_field_name( 'colunm' ); ?>" id="<?php echo $this->get_field_id( 'colunm' ); ?>" class="widefat">
                <option value="2" <?php if($colunm == 2) { echo 'selected'; } ?> >2</option>
                <option value="3" <?php if($colunm == 3) { echo 'selected'; } ?> >3</option>
                <option value="4" <?php if($colunm == 4) { echo 'selected'; } ?> >4</option>
                <option value="6" <?php if($colunm == 6) { echo 'selected'; } ?> >6</option>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Danh mục sản phẩm:', 'dokan' ); ?></label>
            <?php
            $inputField = $this->get_field_name( 'category' );
            $categoryObj = new Categories;
            $categoryObj->getCategory($category, $inputField);
            ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'type' ); ?>"><?php _e( 'Kiểu hiển thị:', 'dokan' ); ?></label>
        <p>
            <input class="widefat"
                   id="<?php echo $this->get_field_id( 'type' ); ?>"
                   name="<?php echo $this->get_field_name( 'type' ); ?>" type="radio"
                   value="0" <?php if($type == 0 || empty($type)) { echo 'checked'; } ?> /><label>Grid</label>
            </br>
            <input class="widefat"
                   id="<?php echo $this->get_field_id( 'type' ); ?>"
                   name="<?php echo $this->get_field_name( 'type' ); ?>" type="radio"
                   value="1" <?php if($type == 1) { echo 'checked'; } ?> /><label>Slide</label>
        </p>
        </p>
        <?php
    }

    /**
     * save widget form
     */

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['post_number'] = strip_tags($new_instance['post_number']);
        $instance['colunm'] = strip_tags($new_instance['colunm']);
        $instance['category'] = (array)$new_instance['category'];
        $instance['type'] = strip_tags($new_instance['type']);
        return $instance;
    }

    /**
     * Show widget
     */

    function widget( $args, $instance ) {
        extract($args);
        $title = $instance['title'];
        $post_number = $instance['post_number'];
        $colunm = $instance['colunm'];
        $category = (array)$instance['category'];
        $category = implode(",",$category);
        $type = $instance['type'];
        echo $before_widget;
        if($type == 0):
            ?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="row home-productlist">
                        <div class="productlist-head">
                            <h2 class="title_category"><span><?= $title ?></span></h2>
                        </div>
                        <?php echo do_shortcode('[product_category per_page="'.$post_number.'" product_num="'.$colunm.'" wrap_class="product-grid productlist" category="'. $category .'" ]'); ?>
                    </div>
                </div>
            </div>
        <?php else: ?>

            <div class="row home-productlist">
                <div class="productlist-head">
                    <h2 class="title_category"><span><?= $title ?></span></h2>
                </div>
                <?php echo do_shortcode('[product_category per_page="'.$post_number.'" slide="proslide-item" category="'. $category .'" wrap_class="product-slider productlist" ]'); ?>
            </div>

        <?php endif; ?>
        <?php
        echo $after_widget;
    }
}

?>