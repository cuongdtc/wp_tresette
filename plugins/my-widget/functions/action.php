<?php

function widget_product_home() {
    register_sidebar( array(
        'name'          => __( 'Home Product', 'dokan' ),
        'id'            => 'home-product',
        'description'   => __( 'Widgets in this area will be shown product in home page.', 'dokan' ),
    ) );
}
add_action( 'widgets_init', 'widget_product_home' );
